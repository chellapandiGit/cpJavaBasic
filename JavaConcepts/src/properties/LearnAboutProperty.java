package properties;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class LearnAboutProperty {
	/**
	 * @param args
	 *            This is one way of loading the manually created properties
	 *            file using FileReader.
	 */
	public static void main(String[] args) {
		File propertyFile = new File("D:\\myTemp\\UserDefinedValues.properties");
		try {
			FileReader proReader = new FileReader(propertyFile);
			Properties userProperty = new Properties();
			userProperty.load(proReader);
			// This is return if the key is available else return Data
			System.out.println(userProperty.getOrDefault(
					"prevalidation.default.screen", "Data"));
			// This one print only matched Key
			System.out
					.println(userProperty.get("prevalidation.default.screen"));
			// If the key is not present it will print null.
			System.out.println(userProperty.get("dat"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
