package properties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class LearnAboutPropertiesType2 {
	public static void main(String[] args) {
		try {
			Properties userProperties = new Properties();
			File propertyFile = new File(
					"D:\\myTemp\\UserDefinedValues.properties");
			OutputStream outStream = new FileOutputStream(propertyFile);
			userProperties.storeToXML(outStream,
					"Extract.Max.Display.Records:90", "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
