package properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class LearnAboutPropertyType1 {

	public static void main(String[] args) {

		try {
			Properties userProperties = new Properties();
			File propertyFile = new File(
					"D:\\myTemp\\UserDefinedValues.properties");
			FileInputStream stream = new FileInputStream(propertyFile);
			userProperties.load(stream);
			// This is return if the key is available else return Data
			System.out.println(userProperties.getOrDefault(
					"prevalidation.default.screen", "Data"));
			// This one print only matched Key
			System.out.println(userProperties
					.get("prevalidation.default.screen"));
			// If the key is not present it will print null.
			System.out.println(userProperties.get("dat"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
